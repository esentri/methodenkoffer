---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.05 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
---
![contain](slides/event-storming/Folie1.png)

(pause: 3)

Hallo und herzlich Willkommen zu einem kurzen Intro über die Methode Event Storming.

(pause: 2)
---
![contain](slides/event-storming/Folie2.png)


Da die Methode bei issentri häufig im Kontext von Domain Driven Design angewandt wird, lasst uns kurz die wichtigsten Begriffe klären. Domain Driven Design ist eine Herangehensweise oder auch ein Konzept der Architektur, deren Zentrum die Implementierung des Domänenmodells ist.

(pause: 1)

Die Domäne ist der Bereich, dessen Probleme mit Hilfe der Software gelöst werden soll. Also der fachliche Kontext in dem wir uns bewegen.

(pause: 1)

Das Domänenmodell bringt System in die Domäne. Zerlegt sie in Elemente und gruppiert solche die zusammengehören in Module. Es macht die zugrunde liegende Idee einer Domäne transparent und schafft eine gemeinsame Basis für Diskussionen.

(pause: 2)
---
![contain](slides/event-storming/Folie3.png)

Nun aber zur Methode: Event Storming. Sie ist sinnvoll um eine Domäne initial zu erforschen. Sehr häufig ganz zu Beginn eines Projektes. Es kommt hier noch nicht auf einzelne Prozesse sondern auf die Events, die Personen und die beteiligten Systeme an.

(pause: 1)

Wichtig ist die Zusammensetzung des Teams. Es braucht die experts der Domäne, die die Events im jeweiligen Kontext kennen und idealerweise alltäglichen Bezug dazu haben. Natürlich braucht es das Team, das neu in der Domäne arbeiten muss. In unserem Kontext also in der Regel das Team das ein Software-Projekt realisiert. Interessant sind auch weitere Stakeholder des Projekts, die die Prozesse der Domäne häufig nur von einer höheren Perspektive aus kennen. Das gemeinsame Arbeiten am Modell schafft Transparenz und Verständnis auf allen Seiten.

(pause: 1)

Ebenfalls sinnvoll ist eine Moderatoren-Rolle um einen Workshop zu begleiten, durch die unterschiedlichen Schritte zu führen und den Zeitplan im Auge zu behalten.

(pause: 1)

Und dann geht es auch schon an die Utensilien. In erster Linie braucht man Platz: Platz zum Modellieren und Platz sich zu bewegen. Eine klare Legende hilft den Überblick zu behalten, denn es wird zunächst beabsichtigt sehr unübersichtlich. Orangene Post-Its eignen sich für Events. Rote Post-Its sind ideal für Hotspots, die anzeigen, dass noch etwas geklärt werden muss.

(pause: 2)
---
![contain](slides/event-storming/Folie4.png)

Nun die einzelnen Schritte durch einen Event Storming Workshop. Das chaotische Erforschen stellt den Anfang dar: es werden Events gesammelt. Sie werden auf Post-Its in der Farbe orange geschrieben. Events werden in der Vergangenheit formuliert. Beispiel: Artikel wurde in Warenkorb gelegt. Hier sind also die experts der Domäne gefragt. Die Events sollten von Beginn an, zumindest grob zeitlich sortiert werden. Links Events die früh, weiter rechts Events die später auftreten.

(pause: 2)
---
![contain](slides/event-storming/Folie5.png)

Parallel und anschließend können alle anderen Rollen Hotspots anbringen. Hotspots werden auf Post-Its in der Farbe rot geschrieben. Sie werden genutzt wenn ein einzelnes Event unklar ist, Prozesse an einer Stelle unklar sind oder es grundsätzlich Verständnisschwierigkeiten gibt. Mit Hilfe der Hotspots werden die gesammelten Events erstmals in der Gruppe diskutiert.

(pause: 1)

In der Regel ist es sinnvoll die ersten beiden Schritte iterativ zu wiederholen.

(pause: 2)
---
![contain](slides/event-storming/Folie6.png)

Herrscht das Gefühl vor, dass die Sammlung an Events vollständig ist werden Pivotal Events definiert. Pivotal Events sind solche, die für den Prozess von besonderer Bedeutung sind. In der Regel weil sie richtig Schwung in den Prozess bringen. Oder auch weil sie ein wichtiger Meilenstein eines Prozesses sind. Ein Beispiel könnte sein: Kunde hat Bestellung aufgegeben.

(pause: 1)
---
![contain](slides/event-storming/Folie7.png)

Pivotal Events werden mit langen, gelben vertikalen Streifen gekennzeichnet. Anhand der Pivotal Events werden die Events zeitlich neu sortiert. Es fällt nun leicht zu sagen welches einzelne Event vor oder nach einem Pivotal Event liegt. Außerdem können Pivotal Events ein Anzeichen für eine Systemgrenze sein.

(pause: 2)
---
![contain](slides/event-storming/Folie8.png)

Ist das geschafft werden User und Systeme zu Events hinzugefügt. User sind Personen in ihrer Rolle, die an einem Event unmittelbar beteiligt sind. Systeme sind daran zu erkennen, dass man sie beschuldigen kann. Es kann eine Maschine, eine Organisation sein, oder sogar das Wetter.

(pause: 1)

User werden in hellgelb - Systeme in lila gekennzeichnet.

(pause: 2)
---
![contain](slides/event-storming/Folie9.png)

Um ein vollständiges und konsistentes Domänenmodell zu bekommen, nutzen wir den Walkthrough. Eine Person führt durch den Prozess, beweget sich die Modellierungsfläche entlang und versucht eine konsistente Geschichte zu erzählen. Dieses Mittel ist sehr hilfreich um unrunde Stellen in der Geschichte zu identifizieren und das Modell zu verbessern. Es ist bei komplexen oder mehreren Prozessen hilfreich dass diese Person rotiert. Auch hier kann mehrmaliges Durchführen sinnvoll sein wenn es an vielen Stellen der Geschichte holpert.

(pause: 2)
---
![contain](slides/event-storming/Folie10.png)

So kann ein Modell inmitten des Walkthrough aussehen. Links ist das Modell bereits überarbeitet.

(pause: 1)

Man erkennt die Events, die Pivotal Events sowie die Systeme.

(pause: 1)

Die erzählende Person geht von links nach rechts, also entlang der zeitlichen Reihenfolge, und erzählt die Geschichte der Domäne.

(pause: 2)
---
![contain](slides/event-storming/Folie1.png)

Ich hoffe, ich konnte Dein Interesse an der Methode Event Storming wecken und dir den Einstieg erleichtern. Bis bald!

(pause: 2)
---
